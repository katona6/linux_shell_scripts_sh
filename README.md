# Linux SHELL SCRIPTS (.SH) #

## brightness.sh ##
`Set Intel VGA Brightness script in laptops. Adjust brightness in tiny little steps with +/- buttons.`
## desktop_session_detector.sh ##
`Detect GUI WindowManager`
## dockedData.sh ##
`Reach Data on External HDD Drives - Automatically unmount & PowerOff HDD after the usage. PowerOn/PowerOff on demand.`
## idokep.sh ##
`Save Weather maps of Hungary in every X minutes from Idokep.hu site.`
## idokep_szmog.sh ##
`Save Weather other infos of Hungary in every X minutes from Idokep.hu site.`
## monitor_set.sh ##
`Set Internal Laptop Screen && outer VGA screen Resolutions GREATER THAN NATIVE res...`
## __wallpaper_player.sh & __wallpaper_random.sh ##
`Sequence Wallpaper player & Random Wallpaper in every X minutes, on Gnome & WindowMaker desktops.`
