#!/bin/bash

# FOR CRON
#PID=$(pgrep gnome-session)
#export DBUS_SESSION_BUS_ADDRESS=$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$PID/environ|cut -d= -f2-)

#WALLS_PATH=/home/katona6/Documents/HATTER			# Munka mappa
WALLS_PATH=/home/katona6/GNUstep/Library/WindowMaker/Backgrounds/FULL
cd $WALLS_PATH							# Munka mappába lépés

#FELTöLT egy wallpapers_array[] tömböt a mappában található fájlok neveivel
i=0;								# Tömb-elem mutató inicializálás
for ACTUAL_WALL in "$WALLS_PATH"/*.*; do
	wallpapers_array[$i]=$ACTUAL_WALL
	i=$(( $i + 1 )) 					# Tömb-elem mutató növelés
done

# echo "${wallpapers_array[@]}"					FEL LETT TöLTVE A TöMB

# ************************* ELöáLLT A wallpapers_array[] TöMB *************************

countervar=0;							# Mutató a beállitandó háttérre DEFAULT=0;

if [ -f ./SZAMLALO/counter ]; then				# Ha van counter mentve
	countervar=`cat ./SZAMLALO/counter`;			# akkor az lesz a beállitandó háttér

	if [ "$countervar" -eq "$i" ]; then			# Ha a beállitandó háttér == TöMB UTOLSó ELEMMEL
		countervar=0;					# akkor a beállitandó háttér visszaáll DEFAULT=0;
	fi	
fi

#gsettings set org.gnome.desktop.background picture-uri "file://${wallpapers_array[countervar]}"
wmsetbg -f "${wallpapers_array[countervar]}"			# HáTTéR BEáLLITáSA


countervar=$(( $countervar + 1 ))				# beállitandó háttér mutasson a következöre
echo $countervar > "./SZAMLALO/counter"				# beállitandó háttér MENTéSE /köv.re mutat/
