#!/bin/bash

# FOR CRON
PID=$(pgrep gnome-session)
export DBUS_SESSION_BUS_ADDRESS=$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$PID/environ|cut -d= -f2-)


# **********************************************get images
files=(/home/katona6/GNUstep/Library/WindowMaker/Backgrounds/FULL/*.*)
#files=(/home/katona6/Documents/HATTER/*.*)
# find out random one
n=${#files[@]}           
wallpaper="${files[RANDOM % n]}"
# **********************************************set image
gsettings set org.gnome.desktop.background picture-uri "file://${wallpaper}"
#wmsetbg -f "$wallpaper"




# DE-detection-sensitive set image
# wmaker command
#if [[ `echo "\$DESKTOP_SESSION"` = *wmaker* ]]; then
#	wmsetbg -f "$wallpaper"
#fi

# metacity
#if [[ `echo "\$DESKTOP_SESSION"` = *metacity* ]]; then
#	gsettings set org.gnome.desktop.background picture-uri "file://${wallpaper}"
#fi

# compiz
#if [[ `echo "\$DESKTOP_SESSION"` = *gnome-flashback-compiz* ]]; then
#	gsettings set org.gnome.desktop.background picture-uri "file://${wallpaper}"
#fi

# ubuntu 
#if [[ `echo "\$DESKTOP_SESSION"` = *ubuntu* ]]; then
#	gsettings set org.gnome.desktop.background picture-uri "file://${wallpaper}"
#fi
