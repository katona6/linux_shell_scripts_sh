#!/bin/bash
echo "Set Intel Brightness script . . . "


# IF There was an argument - set that initial value ; if not start with 2000 as default

if [ $# -eq 0 ]
	then
		br_value=2000;
	else
		br_value=$1;	
fi

echo "$br_value";
sudo su -c "echo $br_value > /sys/class/backlight/intel_backlight/brightness ";

# read key into input inf_cycle
while true; 
	do 	
	read -rsn1 input;
	
	if [[ $input == *+* ]]
		then br_value=$(( $br_value + 20 ));
	fi
	if [[ $input == *-* ]]
		then br_value=$(( $br_value - 20 ));
	fi

	echo "$br_value";
	sudo su -c "echo $br_value > /sys/class/backlight/intel_backlight/brightness ";

done;

 
