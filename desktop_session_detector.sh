#!/bin/bash
if [[ `echo "\$DESKTOP_SESSION"` = *wmaker* ]]; then
	echo "GNUStep WindowMaker Desktop Environment"
fi
if [[ `echo "\$DESKTOP_SESSION"` = *metacity* ]]; then
	echo "GNOME Flashback - Metacity Desktop Environment"
fi
if [[ `echo "\$DESKTOP_SESSION"` = *gnome-flashback-compiz* ]]; then
	echo "GNOME Flashback - Compiz Desktop Environment"
fi
if [[ `echo "\$DESKTOP_SESSION"` = *ubuntu* ]]; then
	echo "UBUNTU Unity Desktop Environment"
fi
