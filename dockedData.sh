#!/bin/bash

DIR="/media/katona6/WD500" ;
PICDIR=$DIR"/__/____/___/" ;

if [ ! -d "$DIR" ]; then
    zenity --info --width=300 --icon-name='dialog-warning' --title="Az elérés nem biztosított" --text="Nem érhetőek el a fájlok!\n<b>Kapcsold be a dokkolót</b>,\nmajd próbáld újra!" ;
else
    # START PICS & save processID
    thunar $PICDIR & 
        export PIDCAJA=$! ;
    #wait for process terminate
    wait $PIDCAJA ;

    #Unmount & powerOff
    PARTITION=$(findmnt -n --mountpoint $DIR | grep /dev | awk '{print $2}') ;
    DEVICE=${PARTITION::-1} ;

    udisksctl unmount -b $PARTITION && sleep 2s &&
    udisksctl power-off -b $DEVICE ;

    #Goodbye
    zenity --info --width=300 --title="Kiléptél a képekből" --text="Most már kikapcsolhatod a dokkolót !" 

fi
