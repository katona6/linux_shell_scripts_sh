#!/bin/bash

# Declarations of Functions()

VGA_set_function() {
	echo "╔════════════════════════════════════════════════╗"
	echo "║               VGA felbontása?		         ║"
	echo "╠════════════════════════════════════════════════╣"
	echo "║ $(tput smso)1.$(tput rmso) $(tput smul)	 800x600  @ 85 Hz$(tput rmul)   $(tput setaf 2)(4:3)$(tput setaf 7)  		 ║" 
	echo "║ $(tput smso)2.$(tput rmso) $(tput smul)	1024x768  @ 85 Hz$(tput rmul)   $(tput setaf 2)(4:3)$(tput setaf 7)		 ║"
	echo "║ $(tput smso)3.$(tput rmso) $(tput smul)	1200x900  @ 85 Hz$(tput rmul)   $(tput setaf 2)(4:3)$(tput setaf 7)		 ║"
	echo "║ $(tput smso)4.$(tput rmso) $(tput smul)	1128x900  @ 85 Hz$(tput rmul)   $(tput setaf 1)(5:4)$(tput setaf 7)		 ║"
	echo "║ $(tput smso)5.$(tput rmso) $(tput smul)	1280x1024 @ 85 Hz$(tput rmul)   $(tput setaf 1)(5:4)$(tput setaf 7)		 ║"
	echo "╠════════════════════════════════════════════════╣"
	echo "║ Nyomj (1) - (2) - (3) - (4) - (5) -t           ║"
	echo "╚════════════════════════════════════════════════╝"

	# read key from input
	read -rsn1 input

	if [[ $input == *1* ]]
	then
		echo "1 -t nyomtál"
		echo "800x600 @ 85 Hz		kiválasztva"
		xrandr --newmode "800x600_85.00"   56.75  800 848 928 1056  600 603 607 633 -hsync +vsync &&
		xrandr --verbose --addmode VGA-1-1 800x600_85.00 &&
		xrandr --output VGA-1-1 --mode 800x600_85.00 &&
		sleep 1s &&
		xrandr --output VGA-1-1 --gamma 1.5:1.5:1.5
		echo "800x600 @ 85 Hz		sikeresen beállítva"
	fi
	if [[ $input == *2* ]]
	then
		echo "2 -t nyomtál"
		echo "1024x768 @ 85 Hz		kiválasztva"
		xrandr --newmode "1024x768_85.00"   94.50  1024 1096 1200 1376  768 771 775 809 -hsync +vsync &&
		xrandr --verbose --addmode VGA-1-1 1024x768_85.00 &&
		xrandr --output VGA-1-1 --mode 1024x768_85.00 &&
		sleep 1s &&
		xrandr --output VGA-1-1 --gamma 1.5:1.5:1.5
		echo "1024x768 @ 85 Hz		sikeresen beállítva"
	fi
	if [[ $input == *3* ]]
	then
		echo "3 -t nyomtál"
		echo "1200x900 @ 85 Hz		kiválasztva"
		xrandr --newmode "1200x900_85.00"  130.00  1200 1280 1408 1616  900 903 907 948 -hsync +vsync &&
		xrandr --verbose --addmode VGA-1-1 1200x900_85.00 &&
		xrandr --output VGA-1-1 --mode 1200x900_85.00 &&
		sleep 1s &&
		xrandr --output VGA-1-1 --gamma 1.5:1.5:1.5
		echo "1200x900 @ 85 Hz		sikeresen beállítva"

	fi
	if [[ $input == *4* ]]
	then
		echo "4 -t nyomtál"
		echo "1128x900 @ 85 Hz		kiválasztva"
		xrandr --newmode "1128x900_85.00"  123.00  1128 1208 1328 1528  900 903 913 948 -hsync +vsync &&
		xrandr --verbose --addmode VGA-1-1 1128x900_85.00 &&
		xrandr --output VGA-1-1 --mode 1128x900_85.00 &&
		sleep 1s &&
		xrandr --output VGA-1-1 --gamma 1.5:1.5:1.5
		echo "1128x900 @ 85 Hz		sikeresen beállítva"

	fi
	if [[ $input == *5* ]]
	then
		echo "5 -t nyomtál"
		echo "1280x1024 @ 85 Hz		kiválasztva"
		xrandr --newmode "1280x1024_85.00"  159.50  1280 1376 1512 1744  1024 1027 1034 1078 -hsync +vsync &&
		xrandr --verbose --addmode VGA-1-1 1280x1024_85.00 &&
		xrandr --output VGA-1-1 --mode 1280x1024_85.00 &&
		sleep 1s &&
		xrandr --output VGA-1-1 --gamma 1.5:1.5:1.5
		echo "1280x1024 @ 85 Hz		sikeresen beállítva"

	fi

# end of VGA_set_function()
}

LCD_set_function() {
	echo "╔════════════════════════════════════════════════╗"
	echo "║          Beépített LCD felbontása ?	         ║"
	echo "╠════════════════════════════════════════════════╣"
	echo "║ $(tput smso)1.$(tput rmso) $(tput smul)	Jelenlegi maradjon$(tput rmul)			 ║"
	echo "║ $(tput smso)2.$(tput rmso) $(tput smul)	1280x720 @ 60 Hz$(tput rmul)	KISEBB		 ║"
	echo "║ $(tput smso)3.$(tput rmso) $(tput smul)	1600x900 @ 60 Hz$(tput rmul)	NAGYOBB		 ║"
	echo "║ $(tput smso)4.$(tput rmso) $(tput smul)	1920x1080 @ 60 Hz$(tput rmul)	NAGYOBB		 ║"
	echo "║ $(tput smso)5.$(tput rmso) $(tput smul)	2560x1440 @ 30 Hz$(tput rmul)	NAGYOBB		 ║"
	echo "╠════════════════════════════════════════════════╣"
	echo "║ Nyomj (1) - (2) - (3) - (4) - (5) -t		 ║"
	echo "╚════════════════════════════════════════════════╝"

	# read key from input
	read -rsn1 input2

	if [[ $input2 == *1* ]]
	then
		echo "1 -t nyomtál"
		echo "Jelenlegi		kiválasztva"
	fi
	if [[ $input2 == *2* ]]
	then
		echo "2 -t nyomtál"
		echo "1280x720 @ 60 Hz	KISEBB		kiválasztva"
		xrandr --newmode "1280x720_60.00"   74.50  1280 1344 1472 1664  720 723 728 748 -hsync +vsync &&
		xrandr --verbose --addmode LVDS-1-1 1280x720_60.00 &&
		xrandr --output LVDS-1-1 --mode 1280x720_60.00 &&
		echo "1280x720 @ 60 Hz	KISEBB		sikeresen beállítva"
	fi
	if [[ $input2 == *3* ]]
	then
		echo "3 -t nyomtál"
		echo "1600x900 @ 60 Hz	NAGYOBB		kiválasztva"
		xrandr --newmode "1600x900_60.00"  118.25  1600 1696 1856 2112  900 903 908 934 -hsync +vsync &&
		xrandr --verbose --addmode LVDS-1-1 1600x900_60.00 &&
		xrandr --output LVDS-1-1 --mode 1600x900_60.00 &&
		echo "1600x900 @ 60 Hz	NAGYOBB		sikeresen beállítva"
	fi
	
	if [[ $input2 == *4* ]]
	then
		echo "4 -t nyomtál"
		echo "1920x1080 @ 60 Hz	NAGYOBB		kiválasztva"
		xrandr --newmode "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync &&
		xrandr --verbose --addmode LVDS-1-1 1920x1080_60.00 &&
		xrandr --output LVDS-1-1 --mode 1920x1080_60.00 &&
		echo "1920x1080 @ 60 Hz	NAGYOBB		sikeresen beállítva"
	fi		
	
	if [[ $input2 == *5* ]]
	then
		echo "5 -t nyomtál"
		echo "2560x1440 @ 30 Hz	NAGYOBB		kiválasztva"
		xrandr --newmode "2560x1440_30.00"  146.25  2560 2680 2944 3328  1440 1443 1448 1468 -hsync +vsync &&
		xrandr --verbose --addmode LVDS-1-1 2560x1440_30.00 &&
		xrandr --output LVDS-1-1 --mode 2560x1440_30.00 &&
		echo "2560x1440 @ 30 Hz	NAGYOBB		sikeresen beállítva"
	fi	
# end of LCD_set_function()
}

#*****************************************************************************
#*****************************************************************************
# MAIN PROGRAM ***************************************************************
#*****************************************************************************
#*****************************************************************************

# clear window
echo "$(tput setab 4) $(tput setaf 7)"
clear &&

# if external VGA detected ...then
if [[ $(xrandr | grep '848') = *848* ]]; then

	echo "Külső VGA észlelve ."
	echo " "
	VGA_set_function
	# Set external VGA resolution succeeded
	# Now set the INTERNAL LCD resolution
	echo " "
	LCD_set_function
	echo "═════════════════════════════════════════════════"
	echo "Sikeres beállítás . Nyomj egy billentyüt a kilépéshez ... $(tput sgr0)"
	read -rsn1 input
	clear # Clear screen before exit

else
# if external VGA NOT DETECTED ...then

	echo "Nem található külsö VGA."
	echo " "
	LCD_set_function
	echo "═════════════════════════════════════════════════"
	echo "Sikeres beállítás . Nyomj egy billentyüt a kilépéshez ... $(tput sgr0)"
	read -rsn1 input
	clear # Clear screen before exit
fi

# end of MAIN PROGRAM ***********************************************************************



